import os
import sys
from subprocess import Popen, PIPE

def get_lattice_path():
    '''find the csns_lattice path firstly'''
    process = Popen(['which', 'tracker.py'], stdout=PIPE, stderr=PIPE)
    stdout, stderr = process.communicate()
    lattice_path = '.'
    if stderr:
        print("Some errors, current path will be used!")
        print(stderr)
        return lattice_path

    if stdout:
        a = stdout.split()
        a=a[0]
        lattice_path = os.path.dirname(a)
    else:
        print("Can't find the lattice information, please make sure csns_lattice"
                " folder is in your PATH environment!")
        print("The lattice path will set to current path!")
        lattice_path = '.'
    return lattice_path


